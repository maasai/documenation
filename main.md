FORMAT: 1A
HOST: https://api url.here

# PasonCal API Documentation
This is public facing information documenting access to different API functions. Please see accompanying document on API server setup.

## Postman Collection

A POSTMAN collection is provided to go with this documentation:

```http
https://www.getpostman.com/collections/xxx
```

## Authorization
All endpoints require authentication through OAuth 2.0 access token (See generate OAuth token in our collection above). To get your token, send a post request to the following endpoint:

```
.../login
```

With the following information:

```
email=your_email
password=your_password
```

If you provide valid information, you should get a response similar to this:

```
{
	"headers": {},
	"original": {
		"accessToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjJkMTIzNTZiNGU0MTE2MGQ2NzU5OGUyMWE5MTczNDgyYTI2Mjc5OWM5MjRhYWVkMDMwMTQ4YWNlNTY0OGIzNTU3NzUyNzJkMGEwYWExMjg1In0.eyJhdWQiOiIyIiwianRpIjoiMmQxMjM1NmI0ZTQxMTYwZDY3NTk4ZTIxYTkxNzM0ODJhMjYyNzk5YzkyNGFhZWQwMzAxNDhhY2U1NjQ4YjM1NTc3NTI3MmQwYTBhYTEyODUiLCJpYXQiOjE1MDQwMDgwNjIsIm5iZiI6MTUwNDAwODA2MiwiZXhwIjoxNTA1MzA0MDYyLCJzdWIiOiI1NjFkNGIzMi04MTA5LTQwNzctYjQ1NS04ODg0NDI2NjMzNjciLCJzY29wZXMiOlsibWFuYWdlLXJlY29yZGluZ3MiLCJtYW5hZ2UtcmVjb3JkaW5nX3RyYW5zY3JpcHRpb25zIiwibWFuYWdlLXJhd190ZXh0cyIsIm1hbmFnZS1yb2xlcyIsIm1hbmFnZS1wZXJtaXNzaW9ucyIsIm1hbmFnZS11c2VycyJdfQ.TQykdVUq4Mqb9_fgnKjlsYBY3ol_54K_UiASOA1rkgw8zURYBdKfNm2pP5l8PMFkmRxDLnLhR09F8Wu-FK0s_dMpK9VgnuCO4k0XdmNP-uaKCeQArF6v4dG2-OapBuJicBAKwH0HUgwZQ-irCuMTHmk-c7jFII0BRiCUlpDk6VywmR1UMrpyGWL_X0LM82kgt3tqKhQMC1Qnp7_DPqzi8B69YjB1LoDgmK0IuBly7kTGwHdB_r185sKWWh77F5RkC27q_dkpOA6k_rocRJdaf46Ig_Yx0P82FiPRn-zBKm3mC_Zr8p94lZEHinspJlPkDyZQO3SwelB0yYmGMh9SZDmH93jUZ3Y_FYX-M3nrQLqwyAiY3XJfeq1z9Su6taq4acXuRbLgitrXLOBOLX8DSYEpCeXuEysuiIfF7NZ_GrZtOiAr3S51q0hPvq13otxYhjuwSs6MEl-46DmTAqCYzAdBit9i3KRM9EFwDbwwmxdTrnG83_Gz_ZXr-i8EBsMWwygsLtC-9DmRFlZ0t9wDO1YroR1IgWmhi03qLTieBO3BoPc-5L43rNSGmV3W9ovXOqweJHFj4o11dCqG0RmvJvcLR1ss2FDqOy3BRYUxcJCtKLBCzVIwjU-KjKWNYTXeNGrWMSEZGtaxdeelivcSEJiY4CYUtz89sLv_jFSnV0Y",
		"accessTokenExpiration": 1295999
	},
	"exception": null
}
```


Sample error message for a wrong user:

```
{
    "headers": {},
    "original": {
        "error": "invalid_credentials",
        "message": "The user credentials were incorrect."
    },
    "exception": null
}
```

You can then use your access_token as a HTTP header on your requests:

```http
Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjJkMTIzNTZiNGU0MTE2MGQ2NzU5OGUyMWE5MTczNDgyYTI2Mjc5OWM5MjRhYWVkMDMwMTQ4YWNlNTY0OGIzNTU3NzUyNzJkMGEwYWExMjg1In0.eyJhdWQiOiIyIiwianRpIjoiMmQxMjM1NmI0ZTQxMTYwZDY3NTk4ZTIxYTkxNzM0ODJhMjYyNzk5YzkyNGFhZWQwMzAxNDhhY2U1NjQ4YjM1NTc3NTI3MmQwYTBhYTEyODUiLCJpYXQiOjE1MDQwMDgwNjIsIm5iZiI6MTUwNDAwODA2MiwiZXhwIjoxNTA1MzA0MDYyLCJzdWIiOiI1NjFkNGIzMi04MTA5LTQwNzctYjQ1NS04ODg0NDI2NjMzNjciLCJzY29wZXMiOlsibWFuYWdlLXJlY29yZGluZ3MiLCJtYW5hZ2UtcmVjb3JkaW5nX3RyYW5zY3JpcHRpb25zIiwibWFuYWdlLXJhd190ZXh0cyIsIm1hbmFnZS1yb2xlcyIsIm1hbmFnZS1wZXJtaXNzaW9ucyIsIm1hbmFnZS11c2VycyJdfQ.TQykdVUq4Mqb9_fgnKjlsYBY3ol_54K_UiASOA1rkgw8zURYBdKfNm2pP5l8PMFkmRxDLnLhR09F8Wu-FK0s_dMpK9VgnuCO4k0XdmNP-uaKCeQArF6v4dG2-OapBuJicBAKwH0HUgwZQ-irCuMTHmk-c7jFII0BRiCUlpDk6VywmR1UMrpyGWL_X0LM82kgt3tqKhQMC1Qnp7_DPqzi8B69YjB1LoDgmK0IuBly7kTGwHdB_r185sKWWh77F5RkC27q_dkpOA6k_rocRJdaf46Ig_Yx0P82FiPRn-zBKm3mC_Zr8p94lZEHinspJlPkDyZQO3SwelB0yYmGMh9SZDmH93jUZ3Y_FYX-M3nrQLqwyAiY3XJfeq1z9Su6taq4acXuRbLgitrXLOBOLX8DSYEpCeXuEysuiIfF7NZ_GrZtOiAr3S51q0hPvq13otxYhjuwSs6MEl-46DmTAqCYzAdBit9i3KRM9EFwDbwwmxdTrnG83_Gz_ZXr-i8EBsMWwygsLtC-9DmRFlZ0t9wDO1YroR1IgWmhi03qLTieBO3BoPc-5L43rNSGmV3W9ovXOqweJHFj4o11dCqG0RmvJvcLR1ss2FDqOy3BRYUxcJCtKLBCzVIwjU-KjKWNYTXeNGrWMSEZGtaxdeelivcSEJiY4CYUtz89sLv_jFSnV0Y
```

Missing, invalid or expired access token will result to unauthenticted error response.

```
{  
	"error": {  
	  "message": "Unauthenticated..",
	  "status_code":401
	}
}
```

## Logout

Send a post request to logout endpoint.

```
.../logout
```

## Pagination

All data response will be an object with data/error for content and is paginated with a **paginator** object. You may use **page** and **limit** url variables to control amount of data.
<br />Example: 
```
.../users?page=2&limit=20
```

## Filter

You can filter the returned data by providing fields as url variables.
<br />Example: To filter users by email and first name.
```
.../users?email=devtest@devtest.com&first_name=devtest
```

## Data Scope

Access control is handled by this API. A user will interract with data differently depending on their role. Unauthorized access is given with response code 403.


<!-- include(user.md) -->
<!-- include(language.md) -->
<!-- include(text_status.md) -->
<!-- include(text_transcription_status.md) -->
<!-- include(text_translation_status.md) -->
<!-- include(recording_transcription_status.md) -->
<!-- include(recording_status.md) -->
<!-- include(transcription_evaluation.md) -->
<!-- include(raw_text.md) -->
<!-- include(text_translation.md) -->
<!-- include(text_transcription.md) -->
<!-- include(recording.md) -->
<!-- include(recording_transcription.md) -->

