# Group RawTexts
RawTexts

## RawTexts List [/raw_texts{?page}{&limit}]

+ Parameters

    + page (optional, integer, `25`) ... The number of the page to return
    + limit (optional, integer, `5`) ... The number of items to show per page

+ Model

    + Headers

            Content-Type: application/json
            X-Request-ID: f72fc914
            X-Response-Time: 4ms

    + Body

			{
			    "data": [
				{
				    "id": "24848e87-5376-4e9a-9dd8-c56f793598b6",
				    "user_id": "561d4b32-8109-4077-b455-888442663367",
				    "text_status_id": "",
				    "language_id": "",
				    "language": {
					"id": null,
					"language_name": null,
					"language_code": null,
					"language_description": null,
					"created_at": null,
					"updated_at": null
				    },
				    "text_content": "original content herex",
				    "created_at": {
					"timezone": "UTC",
					"value": "2017-08-29 21:12:22",
					"pretty_date": "29-Aug-2017",
					"pretty_time": "21:12"
				    },
				    "updated_at": {
					"timezone": "UTC",
					"value": "2017-08-29 21:12:22",
					"pretty_date": "29-Aug-2017",
					"pretty_time": "21:12"
				    }
				}
			    ],
			    "paginator": {
				"total_count": 1,
				"total_pages": 1,
				"current_page": 1,
				"limit": 10,
				"next_page_url": null
			    }
			}

### Fetch All RawTexts [GET]
Get a list of raw_texts.

+ Request (application/json)

	+ Headers

		    Authorization: Bearer 4ttl6wKyHSztV6kEuSVmUuFAxZOXjX3whRoxDLIY

+ Response 200

    [RawTexts List][]

## Single RawText [/raw_texts/{uuid}]
Get an individual raw_text

+ Parameters

    + uuid (required, string, `6890831b-7133-4a45-896a-afc24cb17160`) ... The raw_text ID

+ Model

    + Headers

            Content-Type: application/json

### Get a RawText [GET]
Get a single raw_text.

+ Request (application/json)

	+ Headers

		    Authorization: Bearer 4ttl6wKyHSztV6kEuSVmUuFAxZOXjX3whRoxDLIY

+ Response 404

    + Headers

            Content-Type: application/json

    + Body

            {  
               "error": {  
                  "message": "RawText not found.",
                  "status_code":404
               }
            }

+ Response 200

    + Headers

            Content-Type: application/json

    + Body

			{
			    "data": {
				"id": "24848e87-5376-4e9a-9dd8-c56f793598b6",
				"user_id": "561d4b32-8109-4077-b455-888442663367",
				"text_status_id": "",
				"language_id": "",
				"language": {
					"id": null,
					"language_name": null,
					"language_code": null,
					"language_description": null,
					"created_at": null,
					"updated_at": null
				},
				"text_content": "original content herex",    
				"created_at": {
					"timezone": "UTC",
					"value": "2017-08-29 21:12:22",
					"pretty_date": "29-Aug-2017",
					"pretty_time": "21:12"
				},
				"updated_at": {
					"timezone": "UTC",
					"value": "2017-08-29 21:12:22",
					"pretty_date": "29-Aug-2017",
					"pretty_time": "21:12"
				}
			    }
			}

## Create RawText [/raw_texts]

### Create a RawText [POST]

To create a new raw_text, provide form data for each provided field.

+ Request (application/json)

	+ Headers

		    Authorization: Bearer 4ttl6wKyHSztV6kEuSVmUuFAxZOXjX3whRoxDLIY

			

+ Response 400

			{
			    "message": "There were validation errors",
			    "errors": {
				"status_name": [
				    "The status_name name field is required."
				]
			    }
			} 

+ Response 200

		"Success !! RawText has been created."

## Delete RawText [/raw_texts/{uuid}]
Delete an individual raw_text

+ Parameters

    + uuid (required, string, `93760a0c-24d2-454e-ae33-71b9804b7abf`) ... The raw_text ID

+ Model

    + Headers

            Content-Type: application/json
	

### Delete a RawText [DELETE]
Delete a single RawText.

+ Request

	+ Headers

		    Authorization: Bearer 4ttl6wKyHSztV6kEuSVmUuFAxZOXjX3whRoxDLIY

+ Response 404

    + Headers

            Content-Type: application/json

    + Body

			{
			    "error": {
				"error": true,
				"message": "RawText not deleted",
				"status_code": 404
			    }
			}

+ Response 200

			"Success !! RawText has been deleted."
