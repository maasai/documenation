# Group RecordingStatuses
RecordingStatuses

## RecordingStatuses List [/recording_statuses{?page}{&limit}]

+ Parameters

    + page (optional, integer, `25`) ... The number of the page to return
    + limit (optional, integer, `5`) ... The number of items to show per page

+ Model

    + Headers

            Content-Type: application/json
            X-Request-ID: f72fc914
            X-Response-Time: 4ms

    + Body

			{
			    "data": [
				{
				    "id": "24848e87-5376-4e9a-9dd8-c56f793598b6",
				    "status_name": "NEW",
				    "status_description": null,
				    "created_at": {
					"timezone": "UTC",
					"value": "2017-08-29 21:12:22",
					"pretty_date": "29-Aug-2017",
					"pretty_time": "21:12"
				    },
				    "updated_at": {
					"timezone": "UTC",
					"value": "2017-08-29 21:12:22",
					"pretty_date": "29-Aug-2017",
					"pretty_time": "21:12"
				    }
				}
			    ],
			    "paginator": {
				"total_count": 1,
				"total_pages": 1,
				"current_page": 1,
				"limit": 10,
				"next_page_url": null
			    }
			}

### Fetch All RecordingStatuses [GET]
Get a list of recording_statuses.

+ Request (application/json)

	+ Headers

		    Authorization: Bearer 4ttl6wKyHSztV6kEuSVmUuFAxZOXjX3whRoxDLIY

+ Response 200

    [RecordingStatuses List][]

## Single RecordingStatus [/recording_statuses/{uuid}]
Get an individual recording_status

+ Parameters

    + uuid (required, string, `6890831b-7133-4a45-896a-afc24cb17160`) ... The recording_status ID

+ Model

    + Headers

            Content-Type: application/json

### Get a RecordingStatus [GET]
Get a single recording_status.

+ Request (application/json)

	+ Headers

		    Authorization: Bearer 4ttl6wKyHSztV6kEuSVmUuFAxZOXjX3whRoxDLIY

+ Response 404

    + Headers

            Content-Type: application/json

    + Body

            {  
               "error": {  
                  "message": "RecordingStatus not found.",
                  "status_code":404
               }
            }

+ Response 200

    + Headers

            Content-Type: application/json

    + Body

			{
			    "data": {
				"id": "24848e87-5376-4e9a-9dd8-c56f793598b6",
				"status_name": "NEW",
				"status_description": null,
				"created_at": {
				    "timezone": "UTC",
				    "value": "2017-08-29 21:12:22",
				    "pretty_date": "29-Aug-2017",
				    "pretty_time": "21:12"
				},
				"updated_at": {
				    "timezone": "UTC",
				    "value": "2017-08-29 21:12:22",
				    "pretty_date": "29-Aug-2017",
				    "pretty_time": "21:12"
				}
			    }
			}

## Create RecordingStatus [/recording_statuses]

### Create a RecordingStatus [POST]

To create a new recording_status, provide form data for each provided field.

+ Request (application/json)

	+ Headers

		    Authorization: Bearer 4ttl6wKyHSztV6kEuSVmUuFAxZOXjX3whRoxDLIY

			

+ Response 400

			{
			    "message": "There were validation errors",
			    "errors": {
				"status_name": [
				    "The status_name name field is required."
				]
			    }
			} 

+ Response 200

		"Success !! RecordingStatus has been created."

## Delete RecordingStatus [/recording_statuses/{uuid}]
Delete an individual recording_status

+ Parameters

    + uuid (required, string, `93760a0c-24d2-454e-ae33-71b9804b7abf`) ... The recording_status ID

+ Model

    + Headers

            Content-Type: application/json
	

### Delete a RecordingStatus [DELETE]
Delete a single RecordingStatus.

+ Request

	+ Headers

		    Authorization: Bearer 4ttl6wKyHSztV6kEuSVmUuFAxZOXjX3whRoxDLIY

+ Response 404

    + Headers

            Content-Type: application/json

    + Body

			{
			    "error": {
				"error": true,
				"message": "RecordingStatus not deleted",
				"status_code": 404
			    }
			}

+ Response 200

			"Success !! RecordingStatus has been deleted."
