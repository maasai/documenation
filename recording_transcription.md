# Group RecordingTranscriptions
RecordingTranscriptions

## RecordingTranscriptions List [/recording_transcriptions{?page}{&limit}]

+ Parameters

    + page (optional, integer, `25`) ... The number of the page to return
    + limit (optional, integer, `5`) ... The number of items to show per page

+ Model

    + Headers

            Content-Type: application/json
            X-Request-ID: f72fc914
            X-Response-Time: 4ms

    + Body

			{
			    "data": [
				{
				    "id": "24848e87-5376-4e9a-9dd8-c56f793598b6",
				    "recording_id": "bdd1052c-19c8-4faf-9d43-b7f095d63cd7",
				    "transcriber_id": "dd031ccf-8b3a-4df2-bd95-f7ef3b5ef832",
				    "recording_transcription_status_id": null,
				    "transcription_evaluation_id": null,
				    "transcription_time": null,
				    "recording_transcription_content": "devtest content",
				    "created_at": {
					"timezone": "UTC",
					"value": "2017-08-29 21:12:22",
					"pretty_date": "29-Aug-2017",
					"pretty_time": "21:12"
				    },
				    "updated_at": {
					"timezone": "UTC",
					"value": "2017-08-29 21:12:22",
					"pretty_date": "29-Aug-2017",
					"pretty_time": "21:12"
				    }
				}
			    ],
			    "paginator": {
				"total_count": 1,
				"total_pages": 1,
				"current_page": 1,
				"limit": 10,
				"next_page_url": null
			    }
			}

### Fetch All RecordingTranscriptions [GET]
Get a list of recording_transcriptions.

+ Request (application/json)

	+ Headers

		    Authorization: Bearer 4ttl6wKyHSztV6kEuSVmUuFAxZOXjX3whRoxDLIY

+ Response 200

    [RecordingTranscriptions List][]

## Single RecordingTranscription [/recording_transcriptions/{uuid}]
Get an individual recording_transcription

+ Parameters

    + uuid (required, string, `6890831b-7133-4a45-896a-afc24cb17160`) ... The recording_transcription ID

+ Model

    + Headers

            Content-Type: application/json

### Get a RecordingTranscription [GET]
Get a single recording_transcription.

+ Request (application/json)

	+ Headers

		    Authorization: Bearer 4ttl6wKyHSztV6kEuSVmUuFAxZOXjX3whRoxDLIY

+ Response 404

    + Headers

            Content-Type: application/json

    + Body

            {  
               "error": {  
                  "message": "RecordingTranscription not found.",
                  "status_code":404
               }
            }

+ Response 200

    + Headers

            Content-Type: application/json

    + Body

			{
			    "data": {
				"id": "24848e87-5376-4e9a-9dd8-c56f793598b6",
				"recording_id": "bdd1052c-19c8-4faf-9d43-b7f095d63cd7",
				"transcriber_id": "dd031ccf-8b3a-4df2-bd95-f7ef3b5ef832",
				"recording_transcription_status_id": null,
				"transcription_evaluation_id": null,
				"transcription_time": null,
				"recording_transcription_content": "devtest content",    
				"created_at": {
					"timezone": "UTC",
					"value": "2017-08-29 21:12:22",
					"pretty_date": "29-Aug-2017",
					"pretty_time": "21:12"
				},
				"updated_at": {
					"timezone": "UTC",
					"value": "2017-08-29 21:12:22",
					"pretty_date": "29-Aug-2017",
					"pretty_time": "21:12"
				}
			    }
			}

## Create RecordingTranscription [/recording_transcriptions]

### Create a RecordingTranscription [POST]

To create a new recording_transcription, provide form data for each provided field.

+ Request (application/json)

	+ Headers

		    Authorization: Bearer 4ttl6wKyHSztV6kEuSVmUuFAxZOXjX3whRoxDLIY

			

+ Response 400

			{
			    "message": "There were validation errors",
			    "errors": {
				"status_name": [
				    "The status_name name field is required."
				]
			    }
			} 

+ Response 200

		"Success !! RecordingTranscription has been created."

## Delete RecordingTranscription [/recording_transcriptions/{uuid}]
Delete an individual recording_transcription

+ Parameters

    + uuid (required, string, `93760a0c-24d2-454e-ae33-71b9804b7abf`) ... The recording_transcription ID

+ Model

    + Headers

            Content-Type: application/json
	

### Delete a RecordingTranscription [DELETE]
Delete a single RecordingTranscription.

+ Request

	+ Headers

		    Authorization: Bearer 4ttl6wKyHSztV6kEuSVmUuFAxZOXjX3whRoxDLIY

+ Response 404

    + Headers

            Content-Type: application/json

    + Body

			{
			    "error": {
				"error": true,
				"message": "RecordingTranscription not deleted",
				"status_code": 404
			    }
			}

+ Response 200

			"Success !! RecordingTranscription has been deleted."
