# Group TextTranscriptions
TextTranscriptions

## TextTranscriptions List [/text_transcriptions{?page}{&limit}]

+ Parameters

    + page (optional, integer, `25`) ... The number of the page to return
    + limit (optional, integer, `5`) ... The number of items to show per page

+ Model

    + Headers

            Content-Type: application/json
            X-Request-ID: f72fc914
            X-Response-Time: 4ms

    + Body

			{
			    "data": [
				{
				    "id": "24848e87-5376-4e9a-9dd8-c56f793598b6",
				    "transcriber_id": "dd031ccf-8b3a-4df2-bd95-f7ef3b5ef832",
				    "raw_text_id": "48e93ed9-2cf4-45e2-b0c6-d66018783a42",
				    "text_translation_status_id": null,
				    "language_id": null,
				    "text_transcription_content": "xxxx",
				    "created_at": {
					"timezone": "UTC",
					"value": "2017-08-29 21:12:22",
					"pretty_date": "29-Aug-2017",
					"pretty_time": "21:12"
				    },
				    "updated_at": {
					"timezone": "UTC",
					"value": "2017-08-29 21:12:22",
					"pretty_date": "29-Aug-2017",
					"pretty_time": "21:12"
				    }
				}
			    ],
			    "paginator": {
				"total_count": 1,
				"total_pages": 1,
				"current_page": 1,
				"limit": 10,
				"next_page_url": null
			    }
			}

### Fetch All TextTranscriptions [GET]
Get a list of text_transcriptions.

+ Request (application/json)

	+ Headers

		    Authorization: Bearer 4ttl6wKyHSztV6kEuSVmUuFAxZOXjX3whRoxDLIY

+ Response 200

    [TextTranscriptions List][]

## Single TextTranscription [/text_transcriptions/{uuid}]
Get an individual text_transcription

+ Parameters

    + uuid (required, string, `6890831b-7133-4a45-896a-afc24cb17160`) ... The text_transcription ID

+ Model

    + Headers

            Content-Type: application/json

### Get a TextTranscription [GET]
Get a single text_transcription.

+ Request (application/json)

	+ Headers

		    Authorization: Bearer 4ttl6wKyHSztV6kEuSVmUuFAxZOXjX3whRoxDLIY

+ Response 404

    + Headers

            Content-Type: application/json

    + Body

            {  
               "error": {  
                  "message": "TextTranscription not found.",
                  "status_code":404
               }
            }

+ Response 200

    + Headers

            Content-Type: application/json

    + Body

			{
			    "data": {
				"id": "24848e87-5376-4e9a-9dd8-c56f793598b6",
				"transcriber_id": "dd031ccf-8b3a-4df2-bd95-f7ef3b5ef832",
				"raw_text_id": "48e93ed9-2cf4-45e2-b0c6-d66018783a42",
				"text_translation_status_id": null,
				"language_id": null,
				"text_transcription_content": "xxxx",
				"created_at": {
				    "timezone": "UTC",
				    "value": "2017-08-29 21:12:22",
				    "pretty_date": "29-Aug-2017",
				    "pretty_time": "21:12"
				},
				"updated_at": {
				    "timezone": "UTC",
				    "value": "2017-08-29 21:12:22",
				    "pretty_date": "29-Aug-2017",
				    "pretty_time": "21:12"
				}
			    }
			}

## Create TextTranscription [/text_transcriptions]

### Create a TextTranscription [POST]

To create a new text_transcription, provide form data for each provided field.

+ Request (application/json)

	+ Headers

		    Authorization: Bearer 4ttl6wKyHSztV6kEuSVmUuFAxZOXjX3whRoxDLIY

			

+ Response 400

			{
			    "message": "There were validation errors",
			    "errors": {
				"status_name": [
				    "The status_name name field is required."
				]
			    }
			} 

+ Response 200

		"Success !! TextTranscription has been created."

## Delete TextTranscription [/text_transcriptions/{uuid}]
Delete an individual text_transcription

+ Parameters

    + uuid (required, string, `93760a0c-24d2-454e-ae33-71b9804b7abf`) ... The text_transcription ID

+ Model

    + Headers

            Content-Type: application/json
	

### Delete a TextTranscription [DELETE]
Delete a single TextTranscription.

+ Request

	+ Headers

		    Authorization: Bearer 4ttl6wKyHSztV6kEuSVmUuFAxZOXjX3whRoxDLIY

+ Response 404

    + Headers

            Content-Type: application/json

    + Body

			{
			    "error": {
				"error": true,
				"message": "TextTranscription not deleted",
				"status_code": 404
			    }
			}

+ Response 200

			"Success !! TextTranscription has been deleted."
