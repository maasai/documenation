# Group Users
Users

## Users List [/users{?page}{&limit}]

+ Parameters

    + page (optional, integer, `25`) ... The number of the page to return
    + limit (optional, integer, `5`) ... The number of items to show per page

+ Model

    + Headers

            Content-Type: application/json
            X-Request-ID: f72fc914
            X-Response-Time: 4ms

    + Body

			{
			    "data": [
				{
				    "uuid": "e3b3bfe2-da95-4682-a4b5-5fdc415ce9b6",
				    "first_name": "devtest",
				    "last_name": "devtest",
				    "email": "devtest@devtest.com",
				    "salutation": "Mr.",
				    "phone": "1234456789",
				    "address": "12452 00100 NY",
				    "profile_picture": "",
				    "deleted_at": null,
				    "created_at": {
					"date": "2016-08-08 13:59:33.000000",
					"timezone_type": 3,
					"timezone": "UTC"
				    },
				    "updated_at": {
					"date": "2016-08-08 13:59:33.000000",
					"timezone_type": 3,
					"timezone": "UTC"
				    }
				}
			    ],
			    "paginator": {
				"total_count": 1,
				"total_pages": 1,
				"current_page": 1,
				"limit": 10
			    }
			}

### Fetch All Users [GET]
Get a list of users.

+ Request (application/json)

	+ Headers

		    Authorization: Bearer 4ttl6wKyHSztV6kEuSVmUuFAxZOXjX3whRoxDLIY

+ Response 200

    [Users List][]

## Single User [/users/{uuid}]
Get an individual user

+ Parameters

    + uuid (required, string, `e3b3bfe2-da95-4682-a4b5-5fdc415ce9b6`) ... The user ID

+ Model

    + Headers

            Content-Type: application/json

### Get a User [GET]
Get a single user.

+ Request (application/json)

	+ Headers

		    Authorization: Bearer 4ttl6wKyHSztV6kEuSVmUuFAxZOXjX3whRoxDLIY

+ Response 404

    + Headers

            Content-Type: application/json

    + Body

            {  
               "error": {  
                  "message": "User not found.",
                  "status_code":404
               }
            }

+ Response 200

    + Headers

            Content-Type: application/json

    + Body

			{
			    "data": {
					"uuid": "e3b3bfe2-da95-4682-a4b5-5fdc415ce9b6",
					"first_name": "devtest",
					"last_name": "devtest",
					"email": "devtest@devtest.com",
					"salutation": "Mr.",
					"phone": "1234456789",
					"address": "12452 00100 NY",
					"profile_picture": "",
					"deleted_at": null,
					"created_at": {
					"date": "2016-08-08 13:59:35.000000",
					"timezone_type": 3,
					"timezone": "UTC"
					},
					"updated_at": {
					"date": "2016-08-08 13:59:35.000000",
					"timezone_type": 3,
					"timezone": "UTC"
					}
			    }
			}

## Active User [/me]
Get an logged in user

+ Model

    + Headers

            Content-Type: application/json

### Get Currently Active User [GET]
Get details of currently active user.

+ Request (application/json)

	+ Headers

		    Authorization: Bearer 4ttl6wKyHSztV6kEuSVmUuFAxZOXjX3whRoxDLIY

+ Response 404

    + Headers

            Content-Type: application/json

    + Body

            {  
               "error": {  
                  "message": "User not found.",
                  "status_code":404
               }
            }

+ Response 401

    + Headers

            Content-Type: application/json

    + Body

            {  
               "error": {  
                  "message": "Unauthenticated..",
                  "status_code":401
               }
            }

+ Response 200

    + Headers

            Content-Type: application/json

    + Body

			{
			    "data": {
				"id": "561d4b32-8109-4077-b455-888442663367",
				"first_name": "devtest1",
				"last_name": null,
				"email": "devtest1@devtest.com",
				"salutation": null,
				"phone": null,
				"address": null,
				"role_id": "30e106c9-f48d-4ced-81ea-04c95d4df837",
				"role": {
				    "id": "30e106c9-f48d-4ced-81ea-04c95d4df837",
				    "role_name": "USER",
				    "display_name": "Normal User",
				    "description": "Uploads documents to be translated",
				    "created_at": {
					"timezone": "UTC",
					"value": "2017-08-16 19:32:30",
					"pretty_date": "16-Aug-2017",
					"pretty_time": "19:32"
				    },
				    "updated_at": {
					"timezone": "UTC",
					"value": "2017-08-16 19:32:30",
					"pretty_date": "16-Aug-2017",
					"pretty_time": "19:32"
				    }
				},
				"profile_picture_url": null,
				"created_at": {
				    "timezone": "UTC",
				    "value": "2017-08-16 19:32:31",
				    "pretty_date": "16-Aug-2017",
				    "pretty_time": "19:32"
				},
				"updated_at": {
				    "timezone": "UTC",
				    "value": "2017-08-16 19:32:31",
				    "pretty_date": "16-Aug-2017",
				    "pretty_time": "19:32"
				}
			    }
			}


## Create User [/users]

### Create a User [POST]

To create a new user, provide form data for each provided field.

+ Request (application/json)

	+ Headers

		    Authorization: Bearer 4ttl6wKyHSztV6kEuSVmUuFAxZOXjX3whRoxDLIY

			

+ Response 400

			{
			    "message": "There were validation errors",
			    "errors": {
				"first_name": [
				    "The first name field is required."
				],
				"last_name": [
				    "The last name field is required."
				],
				"email": [
				    "The email field is required."
				],
				"password": [
				    "The password field is required."
				],
				"salutation": [
				    "The salutation field is required."
				],
				"phone": [
				    "The phone field is required."
				],
				"address": [
				    "The address field is required."
				],
				"profile_picture": [
				    "The profile picture field is required."
				]
			    }
			} 

+ Response 200

		"Success !! User has been created."

## Delete User [/users/{uuid}]
Delete an individual user

+ Parameters

    + uuid (required, string, `93760a0c-24d2-454e-ae33-71b9804b7abf`) ... The user ID

+ Model

    + Headers

            Content-Type: application/json
	

### Delete a User [DELETE]
Delete a single User.

+ Request

	+ Headers

		    Authorization: Bearer 4ttl6wKyHSztV6kEuSVmUuFAxZOXjX3whRoxDLIY

+ Response 404

    + Headers

            Content-Type: application/json

    + Body

			{
			    "error": {
				"error": true,
				"message": "User not deleted",
				"status_code": 404
			    }
			}

+ Response 200

			"Success !! User has been deleted."
